import React, { Component } from 'react';
import './App.css';
import Home from './components/Home/Home'
import RecipeList from './components/RecipeList/RecipeList'
import Footer from './components/Footer/Footer'
import TopNav from './components/TopNav/TopNav'
import {BrowserRouter as Router, Route, Switch } from 'react-router-dom'

class App extends Component {

  render() {
    return (
        <Router >
          <Switch>
            <div className="App">
              <TopNav />
              <div className="contentArea" >
                <Route exact path='/' component={Home} />
                <Route path='/recipes' component={RecipeList} />
              </div>
              <Footer />
            </div>
        </Switch>
      </Router>
    )
  }
}

export default App;
