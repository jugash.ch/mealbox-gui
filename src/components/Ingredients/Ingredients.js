import React, { Component } from 'react'
import { Button, Container, ButtonGroup, Row, Col, Alert, NavLink } from "reactstrap"
import _ from 'underscore'
import './Ingredients.css'

class Ingredients extends Component {
  
  constructor(props) {
    super(props)
    this.onButtonClick = this.onButtonClick.bind(this)

    this.state = {
      selected : [],
      collapse : {}
    }
  }

  onButtonClick(id) {
    this.setState((prevState) => {
      var selected = [...prevState.selected]
      const index = selected.indexOf(id)
      if (index < 0) {
        selected.push(id)
      } else {
        selected.splice(index, 1);
      }
      return {
        selected : selected
      }
    })
  }

  collapseButtons(name) {
    this.setState((prevState) => {
      var collapse = {...prevState.collapse}
      collapse[name]= collapse[name]?!collapse[name]:true
      return {
        collapse : collapse
      }
    })
  }

  render() {
    if(this.props.ingredients) {
      let groups = _.groupBy(this.props.ingredients, 'type')
      let groupDesc = Object.entries(groups).map((group) => {
        let itemDesc = group[1].map((item, idx) => {
          let buttonHTML = 
              <Button 
                  hidden={ !(idx < 10 || this.state.collapse[group[0]])}
                  data-ingredient-name={item.id}
                  key={"ing-"+item.id} 
                  outline={this.state.selected.includes(item.id)?false:true}
                  color="danger"
                  className="ingredient"
                  onClick={() => this.onButtonClick(item.id)}>
                {item.name}
              </Button>
          return buttonHTML
        })
        return (
          <div>
              <Row>
                <Col >
                  <Alert color={"danger"} className={"ingredientPanel"} size={"lg"}>{group[0]}</Alert>
                </Col>
              </Row>
              <Row>  
                  <ButtonGroup>
                    <Col>
                      {itemDesc}
                    </Col>
                  </ButtonGroup>
              </Row>
              <Row>
                <NavLink size="sm" onClick={()=> this.collapseButtons(group[0])}>
                  <p className="ingredientCollapseLink">{this.state.collapse[group[0]]?"<Less":"More>"}</p>
                </NavLink>
              </Row>
          </div>)
      })
      
      return (
        <div>
          <Container>{groupDesc}</Container>
            <Button href="/recipes" color="danger">Show Recipes</Button>
        </div>
      )
    } else {
      return null
    }
  }
}
export default Ingredients