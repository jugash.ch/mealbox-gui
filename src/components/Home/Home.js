import React, { Component } from 'react'
import Axios from 'axios'
import Ingredients from '../Ingredients/Ingredients'

class Home extends Component {

  constructor(props) {
    super(props)
    this.state = {
      ingredients : []
    }
  }

  componentDidMount() {
    Axios.get(process.env.REACT_APP_MB_URL + "/ingredients")
    .then(res => {
      console.log(res.data)
      this.setState({ingredients : res.data})
    })
    .catch(err => console.log(err))
  }

  render() {
    if(this.state.ingredients) {
      return (<div><Ingredients ingredients={this.state.ingredients}/></div>)
    } else {
      return null
    }
  }
}
export default Home