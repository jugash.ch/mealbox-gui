import React, { Component } from 'react'
import { Container, Col, Row, Toast, ToastBody, Badge, Modal, ModalBody, ModalFooter, Button} from 'reactstrap'
import Axios from 'axios'
import RecipeIcon from './RecipeIcon'
import Recipe from './Recipe'
import './RecipeList.css'

class RecipeList extends Component {

  constructor(props) {
    super(props)

    this.toggleModal = this.toggleModal.bind(this)
    this.showRecipeDetails = this.showRecipeDetails.bind(this)

    this.state = {
      recipes: [], 
      isLoading: true,
      modal : false,
    }
  }

  componentDidMount() {
    this.setState({isLoading: true})

    Axios.get(process.env.REACT_APP_MB_URL + "/recipes")
    .then(res => {
      console.log(res.data)
      this.setState({recipes : res.data, isLoading : false})
    })
    .catch(err => console.log(err))
  }

  toggleModal(){
    this.setState({
        modal : !this.state.modal
    })
  }

  showRecipeDetails(recipe) {
    this.setState({
      selectedRecipe : recipe,
    })
    this.toggleModal()
  }

  render() {
    const {recipes, isLoading} = this.state

    if (isLoading) {
      return <p>Loading...</p>
    }


    const recipeList = recipes.map(recipe => {
        return <Col md="4" lg="3" onClick={() => this.showRecipeDetails(recipe)}>
          <div className="recipeCard">
            <RecipeIcon recipe={recipe} />
          </div>
        </Col>
    })

    return (
      <Container fluid>
          <Row>
            <Col md="6">
              <Modal isOpen={this.state.modal} toggle={this.toggleModal} size="lg">
                  <ModalBody>
                    <div className="recipeCard">
                      <Recipe recipe={this.state.selectedRecipe} />
                    </div>
                  </ModalBody>
                  <ModalFooter>
                    <Button color="danger" onClick={this.toggleModal}>Close</Button>{' '}
                  </ModalFooter>
              </Modal>
            </Col>
          </Row>
          <Row>
            <Col>
              <div className="p-3 my-3 rounded">
                <Toast>
                  <ToastBody>
                    <h5>We found <Badge color="danger">{recipes.length}</Badge> recipes with some of your ingredients</h5>
                  </ToastBody>
                </Toast>
              </div>
            </Col>
          </Row>
          <Row md="6" lg="4">
            {recipeList}
          </Row>
      </Container>
    )
  }
}

export default RecipeList