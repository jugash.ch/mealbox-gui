import React,{ Component } from "react"
import { Card, CardImg, CardBody, CardTitle, CardSubtitle, CustomInput, CardText, Badge } from "reactstrap"
import './RecipeList.css'

class Recipe extends Component {

    render() {
        var selectedIngredients = this.props.recipe.ingredient.map((ing) => ing.id)

        return (
            <Card>
                <CardBody>
                    <CardTitle color="danger"><h2 color="danger">{this.props.recipe.name.toUpperCase()}</h2></CardTitle>
                    <br />
                        <h5>Matches <Badge color="danger">{selectedIngredients.length}</Badge> ingredients out of a total 
                               of <Badge color="secondary">{this.props.recipe.ingredient.length}</Badge></h5>
                    <br />                               
                    <CardSubtitle>{this.props.recipe.ingredient.map((ingredient) => {
                        return (
                            <div>
                                <CustomInput id={"rec-ing-"+this.props.recipe.id+"-"+ingredient.id} className="ingredientCheck"
                                    type="checkbox" checked={selectedIngredients && selectedIngredients.includes(ingredient.id)} disabled={true}>
                                    {ingredient.name}
                                </CustomInput>
                            </div>
                        )
                        })
                    }
                    </CardSubtitle>
                    <br />
                    <CardText>
                        <h4>Steps</h4>
                        {
                            this.props.recipe.steps.map((step, idx) => {
                                return <p>{idx+1} {step.description}</p>
                            })
                        }
                    </CardText>
                    <CardImg top src={this.props.recipe.image} alt={this.props.recipe.name} />
                </CardBody>
            </Card>
        )
    }
}
export default Recipe
