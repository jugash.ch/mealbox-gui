import React,{ Component } from "react"
import { Card, CardImg, CardBody, CardTitle, CardSubtitle, Badge } from "reactstrap"
import './RecipeList.css'

class RecipeIcon extends Component {

    render() {
        var selectedIngredients = this.props.recipe.ingredient.map((ing) => ing.id)
        return (
            <Card>
                <CardImg top src={this.props.recipe.image} alt={this.props.recipe.name} />
                <CardBody>
                    <CardTitle><h3>{this.props.recipe.name.toUpperCase()}</h3></CardTitle>
                    <CardSubtitle> Matches <Badge color="danger">{selectedIngredients.length}</Badge> of your selections</CardSubtitle>
                </CardBody>
            </Card>
        )
    }
}
export default RecipeIcon
