import React from 'react';
import { Navbar, NavbarBrand } from 'reactstrap'
import logo from './logo.png'
import './TopNav.css'

class TopNav extends React.Component {

  render() {
    return (
      <Navbar bg="dark">
        <NavbarBrand href="/" >
            <img src={logo} alt="MealBox"/>
        </NavbarBrand>
      </Navbar>)
  }
}

export default TopNav