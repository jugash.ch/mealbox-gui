module.exports = function(){
    var faker = require("faker");
    var _ = require("lodash");
    var units = ["cup", "tsp", "tblsp", "gm", "ml", "oz"]
    var types = ["Dairy", "Vegetables", "Fruits", "Grains", "Meats", "Spices", "Seetners", "Seafood", "Nuts", "Desserts"]
    var tags = ["Breakfast", "Lunch", "Dinner", "Snack", "Dessert"]
    var factor = 5

    var ingredients = _.times(10 * factor, function(n){
        return{
            id: n,
            name : faker.hacker.noun().toLocaleLowerCase(),
            amount : faker.random.number(100),
            units : units[faker.random.number(units.length-1)],
            type : types[faker.random.number(types.length-1)]
        }
    })

    return {
        ingredients : ingredients,
        recipes : _.times(10*factor, function(n) {
            var start = faker.random.number(10)
            var step = factor*(1 + faker.random.number(3))
            var end = start + step + faker.random.number(ingredients.length-1-start-step)
            var selectedIngredients = []
            var idx = 0
            for (let index = start; index < end; index += step) {
                selectedIngredients[idx++] = ingredients[index]
            }

            var name = faker.hacker.adjective().toLocaleLowerCase()

            return  {
                    name: name,
                    id: faker.random.uuid(),
                    description: faker.lorem.paragraph(),
                    tag : tags[faker.random.number(tags.length-1)],
                    image : "https://source.unsplash.com/400x400/?food&sig=" + Math.floor(Math.random()*10),
                    // image: "https://dummyimage.com/300x200/dc3545/fff.jpg&text="+name,
                    ingredient: selectedIngredients,
                    steps: _.times(2+faker.random.number(5), function(m) {
                        return { description: faker.lorem.paragraph()}
                    })
                }
            }
        )
    }
}